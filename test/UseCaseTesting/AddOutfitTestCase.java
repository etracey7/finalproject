package UseCaseTesting;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

import my.closet.Outfit;
import my.closet.database.DBOutfits;
import my.closet.services.OutfitService;

class AddOutfitTestCase {

	public Outfit setUp() {
		Outfit newO = new Outfit();
		String[] items = {"a","b","c"};
		newO.setTitle("Sunday Brunch");
		newO.setItemList(Arrays.asList(items));
		DBOutfits db = new DBOutfits();
		db.deleteOutfit(newO.getTitle());
		return newO;
	}
	
	@Test
	void testValidInput() {
		OutfitService service = OutfitService.getInstance();
		Outfit newO = setUp();
		String[] items = {"a","b","c"};
		service.insertNewOutfit(newO.getTitle(), items);
		//test in outfit is in outfit list
		assertTrue(service.getListNames().contains(newO.getTitle()));
	}

}
