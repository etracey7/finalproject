package UseCaseTesting;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Date;

import org.junit.jupiter.api.Test;

import my.closet.ItemHistory;
import my.closet.services.HistoryService;

class UpdateHistoryCase {
	
	public ItemHistory setUp() {
		ItemHistory newH = new ItemHistory();
		newH.setHistory("wore to event");
		newH.setLastWorn(new Date());
		newH.setName("BU Shirt");
		return newH;
	}

	@Test
	void testInsert() {
		HistoryService service = HistoryService.getInstance();
		ItemHistory newH = setUp();
		service.insertOrUpdate(newH.getName(), newH.getComment(), newH.getLastWorn());
		//checks if history is in list
		assertTrue(service.getListNames().contains(newH.getName()));
	}

}
