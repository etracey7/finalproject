package UseCaseTesting;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import my.closet.ClothingItem;
import my.closet.services.ClosetService;

class AddItemTesting {
	
	ClosetService service = ClosetService.getInstance();
	
	public ClothingItem setUp() {
		ClothingItem newItem = new ClothingItem("Pants", "Cool blue jeans", "Navy","American","New" );
		service.deleteItem("Cool blue jeans");
		return newItem;
	}

	@Test
	void testValidAdd() {
		ClothingItem newItem = setUp();
		service.insertItem(newItem.getDescriptor(), newItem.getBrand(), newItem.getColor(), newItem.getType(), newItem.getDescriptor());
		assertTrue(service.validCheckDescriptor(newItem.getDescriptor()) == false);
	}

}
