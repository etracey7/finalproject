package my.closet;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import my.closet.database.DBConnector;
import my.closet.database.DBHistory;
import my.closet.factory.DBFactory;

/**
 * This class represents the collection of histories that exist in the closet
 * @author Emily
 *
 */
public class HistoryList {
	List<ItemHistory> list;
	int size;
	
	/**
	 * Loads all values from the table 'History' into the list
	 */
	public void load() {
		list = new ArrayList<ItemHistory>();
		DBConnector db = new DBFactory().getDB("HISTORY");
		size = db.init(this);	
	}

	/**
	 * Adds item to the list, called when loading the database
	 * @param history to be added
	 */
	public void addItem(ItemHistory h) {
		list.add(h);
	}
	
	/**
	 * Returns the information of all histories in the list
	 */
	public String toString() {
		String result = "";
		for (ItemHistory i: list) {
			result += "\n" + i.getName() + ": \n" + i.getLastWorn() 
				+ "\nComment: " + i.getComment()+"\n";
		}
		return result;
	}

	/**
	 * searches for the item name in the list
	 * @param item name
	 * @return true if name already exists in list
	 */
	public boolean find(String name) {
		for (ItemHistory i: list) {
			if(i.getName().equals(name)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Adds a new item to the list and calls to add to the database
	 * @param name
	 * @param comment
	 * @param lastWorn
	 */
	public void addItem(String name, String comment, Date lastWorn) {
		ItemHistory newI = new ItemHistory();
		newI.setHistory(comment);
		newI.setLastWorn(lastWorn);
		newI.setName(name);
		list.add(newI);
		DBConnector db = new DBFactory().getDB("HISTORY");
		db.insert(newI);
		size++;
	}

	/**
	 * Updates the item history at the given name
	 * @param name
	 * @param comment
	 * @param lastWorn
	 */
	public void update(String name, String comment, Date lastWorn) {
		DBConnector db = new DBFactory().getDB("HISTORY");
		db.update(name,comment, new java.sql.Date(lastWorn.getTime()));
		for (ItemHistory i: list) {
			if(i.getName().equals(name)) {
				i.setLastWorn(lastWorn);
				i.setHistory(comment);
			}
		}
	}

	/**
	 * deletes a history from the list and calls to delete from database
	 * @param name
	 */
	public void deleteHistory(String name) {
		DBConnector db = new DBFactory().getDB("HISTORY");
		db.delete(name);
		for (int i = 0; i < size; i++) {
			if(list.get(i).getName().equals(name)) {
				list.remove(list.get(i));
				size--;
			}
		}
	}
}
