package my.closet.factory;

import my.closet.database.DBCloset;
import my.closet.database.DBConnector;
import my.closet.database.DBHistory;
import my.closet.database.DBOutfits;

/**
 * Creates a connector based on what element connector should handle
 * @author Emily
 *
 */
public class DBFactory {
	
	public DBConnector getDB(String dbType) {
		if (dbType == null) {
			return null;
		}
		if (dbType.equalsIgnoreCase("CLOSET")) {
			return new DBCloset();
		}
		if (dbType.equalsIgnoreCase("HISTORY")) {
			return new DBHistory();
		}
		if (dbType.equalsIgnoreCase("OUTFITS")) {
			return new DBOutfits();
		}
		
		return null;
	}
}
