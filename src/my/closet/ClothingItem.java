package my.closet;


public class ClothingItem {
	private String type;
	private String descriptor;
	private String color;
	private String brand;
	private String condition;
	
	/**
	 * Constructor for item that initializes all fields
	 * @param type
	 * @param descriptor
	 * @param color
	 * @param brand
	 * @param condition
	 */
	public ClothingItem(String type, String descriptor, String color, String brand, String condition) {
		super();
		this.type = type;
		this.descriptor = descriptor;
		this.color = color;
		this.brand = brand;
		this.condition = condition;
	}
	public ClothingItem() {
		descriptor = null;
	}
	public String getCondition() {
		return condition;
	}
	public void setCondition(String condition) {
		this.condition = condition;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDescriptor() {
		return descriptor;
	}
	public void setDescriptor(String descriptor) {
		this.descriptor = descriptor;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	/**
	 * @return the item with all fields
	 */
	public String toString() {
		String result = "";
		result += "Type: " + type
				+ "\nCondition: " + condition
				+ "\nBrand: " + brand
				+ "\nColor: " + color;
		return result;
	}
}
