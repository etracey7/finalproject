package my.closet.graphics;

import static javax.swing.GroupLayout.Alignment.BASELINE;
import static javax.swing.GroupLayout.Alignment.TRAILING;

import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import my.closet.graphics.builder.AbstractBuilder;
import my.closet.graphics.builder.ButtonBuilder;
import my.closet.graphics.builder.LabelBuilder;
import my.closet.services.HistoryService;

/**
 * window dislayed when a history is to be updated
 * @author Emily
 *
 */
public class AddHistoryPopup extends JFrame {

	private static final long serialVersionUID = 1L;
	private static final int DEFAULT_WIDTH = 300;
	private static final int DEFAULT_HEIGHT = 200;
	
	//current item name
	String currItem;
	
	//Frame Items
	JButton addButton;
	JLabel lbl;
    JLabel lbl2;
    JTextField field = new JTextField("01.01.01");
    JTextField field2 = new JTextField(15);
    Container pane = getContentPane();
    GroupLayout gl = new GroupLayout(pane);
	 
	HistoryService histories = HistoryService.getInstance();
	
	public AddHistoryPopup(String itemName) {
         currItem = itemName;
         initUI();
    }

	/**
	 * initializes the frame
	 */
	private void initUI() {
		createComponents();
		createLayout();
        
		//setting frame settings
        setTitle(currItem+" history");
        setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        getContentPane().setBackground(Color.DARK_GRAY);
    }
	/**
	 * creates the layout of the frame with the components
	 */
	public void createLayout() {
		pane.setLayout(gl);
		gl.setAutoCreateGaps(true);
        gl.setAutoCreateContainerGaps(true);

        //setting across
        gl.setHorizontalGroup(gl.createSequentialGroup()
                .addGroup(gl.createParallelGroup(TRAILING)
                        .addComponent(lbl)
                        .addComponent(lbl2))
                .addGroup(gl.createParallelGroup()
                        .addComponent(field)
                        .addComponent(field2)
                        .addComponent(addButton))
        );

        //setting up-down
        gl.setVerticalGroup(gl.createSequentialGroup()
                .addGroup(gl.createParallelGroup(BASELINE)
                        .addComponent(lbl)
                        .addComponent(field))
                .addGroup(gl.createParallelGroup(BASELINE)
                        .addComponent(lbl2)
                        .addComponent(field2))
                .addComponent(addButton)
        );

        pack();
	}
	/**
	 * initializes the JFrame components
	 */
	public void createComponents() {
		AbstractBuilder b = new ButtonBuilder();
		AbstractBuilder l = new LabelBuilder();
		
        lbl = l.buildNew().buildBackColor().buildText("Last Worn: ").buildTextColor().getLabel();
        lbl2 = l.buildNew().buildBackColor().buildText("Comment: ").buildTextColor().getLabel();
        

        addButton = b.buildNew().buildBackColor().buildText("Update History").buildTextColor().getButton();
        addButton.addActionListener(new ActionListener(){
     	   public void actionPerformed(ActionEvent ae){
     		   		String comment = field2.getText();
     		   		DateFormat format = new SimpleDateFormat("MM.dd.yy"); 
     		   		try {
							Date lastWorn = format.parse(field.getText());
							histories.insertOrUpdate(currItem, comment, lastWorn);
						} catch (ParseException e) {
							System.out.print("NO DATE");
							e.printStackTrace();
						}
     		   		dispose();
     		   }
        });

	}
}
