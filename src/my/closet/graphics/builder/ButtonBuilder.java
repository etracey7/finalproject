package my.closet.graphics.builder;

import java.awt.Color;

import javax.swing.JButton;

/**
 * builds a JButton component using AbstractBuilder methods
 * @author Emily
 *
 */
public class ButtonBuilder extends AbstractBuilder {
	JButton button;
	
	public JButton getButton() {
		return button;
	}
	
	public ButtonBuilder buildNew() {
		button = new JButton();
		return this;
	}
	public ButtonBuilder buildBackColor() {
		button.setBackground(Color.PINK);
		return this;
	}
	public ButtonBuilder buildTextColor() {
		button.setForeground(Color.WHITE);
		return this;
	}
	public ButtonBuilder buildText(String text) {
		button.setText(text);
		return this;
	}
}
