package my.closet.graphics.builder;

import java.awt.Color;

import javax.swing.JLabel;

/**
 * builds a JLabel component by AbstarctBuilder methods
 * @author Emily
 *
 */
public class LabelBuilder extends AbstractBuilder {

	JLabel lbl = null;
	
	public JLabel getLabel() {
		return lbl;
	}
	
	@Override
	public AbstractBuilder buildNew() {
		lbl = new JLabel();
		return this;
	}

	@Override
	public AbstractBuilder buildText(String text) {
		lbl.setText(text);
		return this;
	}

	@Override
	public AbstractBuilder buildBackColor() {
		lbl.setOpaque(true);
		lbl.setBackground(Color.DARK_GRAY);
		return this;
	}

	@Override
	public AbstractBuilder buildTextColor() {
		lbl.setForeground(Color.PINK);
		return this;
	}
	
}
