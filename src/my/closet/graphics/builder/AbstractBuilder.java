package my.closet.graphics.builder;

import javax.swing.JButton;
import javax.swing.JLabel;

/**
 * This class is designed to build JFrame components
 * @author Emily
 *
 */
public abstract class AbstractBuilder {
	/**
	 * creates new element
	 * @return this
	 */
	public abstract AbstractBuilder buildNew();
	/**
	 * assigns component text
	 * @param text
	 * @return this
	 */
	public abstract AbstractBuilder buildText(String text);
	/**
	 * builds the background of a component
	 * @return this
	 */
	public abstract AbstractBuilder buildBackColor();
	/**
	 * builds the text color
	 * @return this
	 */
	public abstract AbstractBuilder buildTextColor();
	/**
	 * return a button component
	 * @return
	 */
	public JButton getButton() {
		return null;}
	/**
	 * returns a label component
	 * @return
	 */
	public JLabel getLabel() {
		return null;}
}
