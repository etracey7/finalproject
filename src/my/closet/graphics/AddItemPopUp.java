package my.closet.graphics;

import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import my.closet.graphics.builder.AbstractBuilder;
import my.closet.graphics.builder.ButtonBuilder;
import my.closet.graphics.builder.LabelBuilder;
import my.closet.services.ClosetService;

import javax.swing.*;

import static javax.swing.GroupLayout.Alignment.BASELINE;
import static javax.swing.GroupLayout.Alignment.TRAILING;
/**
 * window for inserting a ClothingItem
 * @author Emily
 *
 */
public class AddItemPopUp extends JFrame {
	
	private static final long serialVersionUID = 1L;
	private static final int DEFAULT_WIDTH = 500;
	private static final int DEFAULT_HEIGHT = 300;
	
	ClosetService closet = ClosetService.getInstance();
	
	//Frame Items
	JButton addButton;
	JButton cancel;
	JComboBox<String> type = new JComboBox<String>();
	JComboBox<String> condition = new JComboBox<String>();
	
	public AddItemPopUp() {
	    initUI();
	}
	
	/**
	 * initializes the frame
	 */
	private void initUI() {
		
		Container pane = getContentPane();
	    GroupLayout gl = new GroupLayout(pane);
	    pane.setLayout(gl);
	    
	    //assign fields
	    JTextField field = new JTextField(15);
	    JTextField field3 = new JTextField(15);
	    JTextField field2 = new JTextField(15);
	    
	    AbstractBuilder b = new ButtonBuilder();
	    AbstractBuilder l = new LabelBuilder();
	    
	    //create button
	    addButton = b.buildNew().buildBackColor().buildText("Add Item").buildTextColor().getButton();
	    addButton.addActionListener(new ActionListener(){
	 	   public void actionPerformed(ActionEvent ae){
	 		      String descriptor = field.getText();
	 		      if (closet.validCheckDescriptor(descriptor)) {
	     		      String condit = (String) condition.getSelectedItem();
	     		      String t = (String) type.getSelectedItem();
	     		      String color = field2.getText();
	     		      String brand = field3.getText();
	     		      closet.insertItem(descriptor, brand, color, t, condit);
	     		      JOptionPane.showMessageDialog(new JFrame(), "Item has been added");
	     		      dispose();
	     		      ClosetFrame reload = new ClosetFrame();
	   				  reload.setVisible(true);
	 		      } else {
	 		    	  JOptionPane.showMessageDialog(new JFrame(), "Invalid Item Name. "
	 		    			  + "\nThere is probably already an item with that Descriptor."
	 		    			  + "\nRecheck desired item and input data again");
	 		    	  dispose();
	 		    	  AddItemPopUp reload = new AddItemPopUp();
	 		    	  reload.setVisible(true);
	 		      }
	 		   }
	    });
	    
	    //create button
	    cancel = b.buildNew().buildBackColor().buildTextColor().buildText("Cancel").getButton();
	    cancel.addActionListener(new ActionListener(){
	  	   public void actionPerformed(ActionEvent ae){
		      dispose();
		      ClosetFrame reload = new ClosetFrame();
		      reload.setVisible(true);
		   }
	    });
	    
	    addComboBoxes();
	    
	    //create labels
	    JLabel lbl = l.buildNew().buildBackColor().buildTextColor().buildText("Descriptor:").getLabel();
	    JLabel lbl2 = l.buildNew().buildBackColor().buildTextColor().buildText("Color:").getLabel();
	    JLabel lbl3 = l.buildNew().buildBackColor().buildTextColor().buildText("Brand:").getLabel();
	    JLabel lbl4 = l.buildNew().buildBackColor().buildTextColor().buildText("Type:").getLabel();
	    JLabel lbl5 = l.buildNew().buildBackColor().buildTextColor().buildText("Condition:").getLabel();
	    
	    //create layout
	    gl.setAutoCreateGaps(true);
	    gl.setAutoCreateContainerGaps(true);
	
	    gl.setHorizontalGroup(gl.createSequentialGroup()
	            .addGroup(gl.createParallelGroup(TRAILING)
	                    .addComponent(lbl)
	                    .addComponent(lbl2)
	                    .addComponent(lbl3)
	                    .addComponent(lbl4)
	                    .addComponent(lbl5))
	            .addGroup(gl.createParallelGroup()
	                    .addComponent(field)
	                    .addComponent(field2)
	                    .addComponent(field3)
	                    .addComponent(type)
	                    .addComponent(condition)
	                    .addComponent(addButton)
	                    .addComponent(cancel))
	    );
	
	    gl.setVerticalGroup(gl.createSequentialGroup()
	            .addGroup(gl.createParallelGroup(BASELINE)
	                    .addComponent(lbl)
	                    .addComponent(field))
	            .addGroup(gl.createParallelGroup(BASELINE)
	                    .addComponent(lbl2)
	                    .addComponent(field2))
	            .addGroup(gl.createParallelGroup(BASELINE)
	                    .addComponent(lbl3)
	                    .addComponent(field3))
	            .addGroup(gl.createParallelGroup(BASELINE)
	                    .addComponent(lbl4)
	                    .addComponent(type))
	            .addGroup(gl.createParallelGroup(BASELINE)
	                    .addComponent(lbl5)
	                    .addComponent(condition))
	            .addComponent(addButton)
	            .addComponent(cancel)
	    );
	
	    pack();
	   
	    //set settings
	    setTitle("Add Item");
	    setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
	    setLocationRelativeTo(null);
	    setDefaultCloseOperation(HIDE_ON_CLOSE);
	    getContentPane().setBackground(Color.DARK_GRAY);
	}
	
	/**
	 * builds the combo boxes
	 */
	private void addComboBoxes() {
	    type.addItem("Pants");
	    type.addItem("Skirt");
	    type.addItem("Dress");
	    type.addItem("Top");
	    type.addItem("Shoes");
	    type.addItem("Jacket");
	    type.addItem("Undergarment");
	    type.addItem("Jewlery");
	    type.addItem("Other");
	    
	    condition.addItem("New");
	    condition.addItem("Good");
	    condition.addItem("Okay");
	    condition.addItem("Poor");
	    condition.addItem("Unwearable");
	    condition.addItem("Other");
	}
}
