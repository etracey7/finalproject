package my.closet.graphics;

import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

import my.closet.graphics.builder.AbstractBuilder;
import my.closet.graphics.builder.ButtonBuilder;
import my.closet.graphics.builder.LabelBuilder;
import my.closet.services.ClosetService;
import my.closet.services.HistoryService;
import my.closet.services.OutfitService;

public class ClosetFrame extends JFrame {
	
	private static final long serialVersionUID = 1L;
	private static final int DEFAULT_WIDTH = 300;
	private static final int DEFAULT_HEIGHT = 500;
	
	private DefaultListModel<String> model;
	private JList<String> list;
	
	private ClosetService closet = ClosetService.getInstance();
	private OutfitService outfitList = OutfitService.getInstance();
	private HistoryService historyList = HistoryService.getInstance();

	public ClosetFrame() {
       initUI();
	}
	/**
	 * initializes the frame
	 */
	private void initUI() {
		createMenuBar();
		createList();
		
		JScrollPane scrollPane = new JScrollPane(list);
		
		AbstractBuilder b = new ButtonBuilder();
		AbstractBuilder l = new LabelBuilder();
		
		//build buttons
		JButton button = b.buildNew().buildBackColor().buildText("Add History").buildTextColor().getButton();
		button.addActionListener(new ActionListener(){
        	   public void actionPerformed(ActionEvent ae){
        		   if (list.isSelectionEmpty()) {
        			  JOptionPane.showMessageDialog(new JFrame(), "Select an Item");
        		  } else {
        		      String itemName = (String) list.getSelectedValue();
        		      AddHistoryPopup addH = new AddHistoryPopup(itemName);
        		      addH.setVisible(true);
        		  }
        	   }
        });
		
		JButton deleteButton = b.buildNew().buildBackColor().buildText("Delete Item").buildTextColor().getButton();
		deleteButton.addActionListener(new ActionListener(){
        	   public void actionPerformed(ActionEvent ae){
        		  if (list.isSelectionEmpty()) {
        			  JOptionPane.showMessageDialog(new JFrame(), "Select an Item");
        		  } else {
        		      dispose();
        		      String name = (String)list.getSelectedValue();
        		      closet.deleteItem(name);
        		      historyList.deleteItemHisory(name);
        		      ClosetFrame reload = new ClosetFrame();
        		      reload.setVisible(true);
        		  }
    		   }
        });
		
	    JLabel area = l.buildNew().buildBackColor().buildText("Your Clothing Items: ").buildTextColor().getLabel();

	    createLayout(scrollPane, button, area, deleteButton);
	    
	    pack();
	    
	    //set frame settings
	    setTitle("MyCloset");
	    setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
	    setLocationRelativeTo(null);
	    setDefaultCloseOperation(EXIT_ON_CLOSE);
	    getContentPane().setBackground(Color.DARK_GRAY);
	}
    
	/**
	 * initalizes the menu
	 */
    private void createMenuBar() {
    	JMenuBar menubar = new JMenuBar();

        JMenu file = new JMenu("MENU");
        JMenu view = new JMenu("VIEW");
        JMenu sort = new JMenu("SORT");
        

        JMenuItem eMenuItem = new JMenuItem("Exit");
        eMenuItem.setMnemonic(KeyEvent.VK_E);
        eMenuItem.setToolTipText("Exit application");
        eMenuItem.addActionListener((ActionEvent event) -> {
            System.exit(0);
        });
        
        JMenuItem addMenuItem = new JMenuItem("Add Cloting Item");
        addMenuItem.setToolTipText("Add Items to your closet");
        addMenuItem.addActionListener((ActionEvent event) -> {
        	dispose();
	        AddItemPopUp frame = new AddItemPopUp();
			frame.setVisible(true);
        });

        JMenuItem outfitMenuItem = new JMenuItem("Add New Outfit");
        outfitMenuItem.setToolTipText("Add Outfits to your closet");
        outfitMenuItem.addActionListener((ActionEvent event) -> {
        	dispose();
        	AddOutfitPopup outfitFrame = new AddOutfitPopup();
        	outfitFrame.setVisible(true);
        });

        file.add(eMenuItem);
        file.add(addMenuItem);
        file.add(outfitMenuItem);
        
        JMenuItem outfits = new JMenuItem("Outfits");
        outfits.setToolTipText("View Outfits in your closet");
        outfits.addActionListener((ActionEvent event) -> {
        	ViewListPopUp outfitView = new ViewListPopUp(outfitList.getListNames());
        	outfitView.setVisible(true);
        });
        
        JMenuItem histories = new JMenuItem("Histories");
        histories.setToolTipText("View History for items in your closet");
        histories.addActionListener((ActionEvent event) -> {
        	ViewListPopUp historyView = new ViewListPopUp(historyList.getListNames());
        	historyView.setVisible(true);
        });
        
        view.add(outfits);
        view.add(histories);
        
        JMenuItem viewName = new JMenuItem("By Name");
        viewName.setToolTipText("Sort items by Name");
        viewName.addActionListener((ActionEvent event) -> {
        	dispose();
        	closet.sortByName();
        	ClosetFrame reload = new ClosetFrame();
        	reload.setVisible(true);
        });
        
        JMenuItem viewCondit = new JMenuItem("By condition");
        viewCondit.setToolTipText("Sort items by Condition");
        viewCondit.addActionListener((ActionEvent event) -> {
        	dispose();
        	closet.sortByCondition();
        	ClosetFrame reload = new ClosetFrame();
        	reload.setVisible(true);
        });
        
        sort.add(viewName);
        sort.add(viewCondit);

        menubar.add(file);
        menubar.add(view);
        menubar.add(sort);

        menubar.setBackground(Color.PINK);
        menubar.setForeground(Color.DARK_GRAY);
  
        
        setJMenuBar(menubar);
    }
    /**
     * creates the list
     */
    private void createList() {
        model = new DefaultListModel<String>();
        for (int i = 0; i < closet.getNumber(); i++) {
        	model.addElement(closet.getItemTitle(i));
        }

        list = new JList<String>(model);
        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        
        list.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                JList list1 = (JList)evt.getSource();
                if (evt.getClickCount() == 2) {
                    JOptionPane.showMessageDialog(new JFrame(), closet.toString((String)list.getSelectedValue()));
                } 
            }
        });
        
        list.setToolTipText("double click to see information");

    }
    /**
     * create the layout with given components
     * @param arg - components
     */
    private void createLayout(JComponent... arg) {

        Container pane = getContentPane();
        GroupLayout gl = new GroupLayout(pane);
        pane.setLayout(gl);

        gl.setAutoCreateContainerGaps(true);
        gl.setAutoCreateGaps(true);

        gl.setHorizontalGroup(gl.createSequentialGroup()
        		.addGroup(gl.createParallelGroup(GroupLayout.Alignment.LEADING)
        		.addComponent(arg[2])
        		.addComponent(arg[0])
                .addComponent(arg[1])
                .addComponent(arg[3]))
        );

        gl.setVerticalGroup(gl.createSequentialGroup()
                .addComponent(arg[2])
                .addComponent(arg[0])
                .addComponent(arg[1])
                .addComponent(arg[3])
        );

        pack();
    }
}
