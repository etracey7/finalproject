package my.closet.graphics;

import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;

import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;

import my.closet.graphics.builder.AbstractBuilder;
import my.closet.graphics.builder.ButtonBuilder;
import my.closet.services.ClosetService;
import my.closet.services.OutfitService;

public class AddOutfitPopup extends JFrame {

	private static final long serialVersionUID = 1L;
	private static final int DEFAULT_WIDTH = 600;
	private static final int DEFAULT_HEIGHT = 400;
	
	ClosetService closet = ClosetService.getInstance();
	OutfitService outfitList = OutfitService.getInstance();
	
	private DefaultListModel<String> model;
	private JList<String> list;
	
	//Frame Items
    JButton addButton;
	 @SuppressWarnings("rawtypes")
	JComboBox[] items = new JComboBox[8];
	
	public AddOutfitPopup() {
        initUI();
    }

	/**
	 * initialize the frame
	 */
	private void initUI() {
		createList();
		
		JScrollPane scrollPane = new JScrollPane(list);
		
		JTextField area = new JTextField("Outfit Name");
		
		AbstractBuilder b = new ButtonBuilder();
		
		//build buttons
		JButton cancel = b.buildNew().buildBackColor().buildText("Cancel").buildTextColor().getButton();
		cancel.addActionListener(new ActionListener(){
	    	public void actionPerformed(ActionEvent ae){
				dispose();
				ClosetFrame reload = new ClosetFrame();
				reload.setVisible(true);
     	   }
	    });
		
		JButton addButton = b.buildNew().buildBackColor().buildTextColor().buildText("ADD").getButton();
	    addButton.addActionListener(new ActionListener(){
	    	public void actionPerformed(ActionEvent ae){
	    		if (!list.isSelectionEmpty()) {
					String title = area.getText();
					String[] items = Arrays.copyOf(list.getSelectedValues(), list.getSelectedValues().length, String[].class);
					outfitList.insertNewOutfit(title, items);
	   		        JOptionPane.showMessageDialog(new JFrame(), "Outfit has been added");
					dispose();
					ClosetFrame reload = new ClosetFrame();
					reload.setVisible(true);
	    		} else {
	     		      JOptionPane.showMessageDialog(new JFrame(), "Please select at least 1 item!");
	     		      dispose();
	     		      AddOutfitPopup reload = new AddOutfitPopup();
	     		      reload.setVisible(true);
	    		}
     	   }
	    });
	    
	    createLayout(area, scrollPane, addButton, cancel);
	
	    pack();
	    
	    //set frame settings
	    setTitle("Add Outfit");
	    setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
	    setLocationRelativeTo(null);
	    setDefaultCloseOperation(EXIT_ON_CLOSE);
	    this.getContentPane().setBackground(Color.DARK_GRAY);
	}
    
	//create the list component
    private void createList() {
        model = new DefaultListModel<String>();
        for (int i = 0; i < closet.getNumber(); i++) {
        	model.addElement(closet.getItemTitle(i));
        }

        list = new JList<String>(model);
        list.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    }
    /**
     * create the layout with given components
     * @param arg - components
     */
    private void createLayout(JComponent... arg) {

        Container pane = getContentPane();
        GroupLayout gl = new GroupLayout(pane);
        pane.setLayout(gl);

        gl.setAutoCreateContainerGaps(true);
        gl.setAutoCreateGaps(true);

        gl.setHorizontalGroup(gl.createSequentialGroup()
        		.addGroup(gl.createParallelGroup(GroupLayout.Alignment.LEADING)
	                .addComponent(arg[0]))
        		.addComponent(arg[1])
        		.addComponent(arg[2])
        		.addComponent(arg[3])
        );

        gl.setVerticalGroup(gl.createParallelGroup()
        		.addGroup(gl.createParallelGroup(GroupLayout.Alignment.BASELINE)
        			.addComponent(arg[0])
        			.addComponent(arg[1])
        			.addComponent(arg[2]))
        		.addComponent(arg[3])
        );
    }
}
