package my.closet.graphics;

import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * frame that displays text of a list
 * @author Emily
 *
 */
public class ViewListPopUp extends JFrame {
	
	private static final long serialVersionUID = 1L;
	private static final int DEFAULT_WIDTH = 400;
	private static final int DEFAULT_HEIGHT = 400;
	String text = null;
	
	public ViewListPopUp(String list) {
		text = list;
        initUI();
    }

	/**
	 * initializes the frame
	 */
	private void initUI() {
		JTextArea area = new JTextArea(text);
		area.setBackground(Color.DARK_GRAY);
		area.setForeground(Color.PINK);
		JScrollPane pane = new JScrollPane(area);
		
		add(pane);
		
	    setTitle("View");
	    setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
	    setLocationRelativeTo(null);
	    setDefaultCloseOperation(HIDE_ON_CLOSE);
	    getContentPane().setBackground(Color.DARK_GRAY);
	}
}
