package my.closet.graphics;

import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JLabel;

import my.closet.graphics.builder.AbstractBuilder;
import my.closet.graphics.builder.LabelBuilder;

/**
 * The welcome frame
 * @author Emily
 *
 */
public class OpeningFrame extends JFrame {
	static final long serialVersionUID = 1L;

	public OpeningFrame() {

        initUI();
    }

	/**
	 * initialize the frame
	 */
	private void initUI() {
		AbstractBuilder l = new LabelBuilder();
		JLabel title = l.buildNew().buildBackColor().buildText("Welcome to Your Closet").buildTextColor().getLabel();
	    title.setFont(new Font("Papyrus", Font.ITALIC, 36));
	    add(title);
	    
	    pack();
	    
	    setTitle("MyCloset");
	    setSize(500, 200);
	    setLocationRelativeTo(null);
	    setDefaultCloseOperation(HIDE_ON_CLOSE);
	}
}
