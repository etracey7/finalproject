package my.closet;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import my.closet.database.DBConnector;
import my.closet.factory.DBFactory;

/**
 * This class represents the clothing items stored within a closet
 * @author Emily
 *
 */
public class Closet {
	
	private List<ClothingItem> itemList = new ArrayList<ClothingItem>();
	int size = 0;
	
	/**
	 * This function loads any items stored in the database to the itemList
	 * Sets the size
	 * ItemList may change
	 */
	public void load() {
		DBConnector db = new DBFactory().getDB("CLOSET");
		size = db.init(this);	
	}
	/**
	 * Adds clothing item to itemList and the database
	 * @param Clothing item to be added
	 */
	public void addItem(ClothingItem i) {
		if (!find(i.getDescriptor())) {
			itemList.add(i);
			DBConnector db = new DBFactory().getDB("CLOSET");
			db.addItem(i);
			size++;
		}
	}
	/**
	 * adds the item to only the itemList
	 * meant to be called in the DBCloset to initialize itemList
	 * @param item to be added
	 */
	public void addListItem(ClothingItem i) {
		if (!find(i.getDescriptor())) {
			itemList.add(i);
			size++;
		}
	}
	/**
	 * Searches for an instance of an item in the itemList
	 * @param item to search for
	 * @return whether the item is already in the itemList
	 */
	public boolean find(String d) {
		for (ClothingItem item : itemList) {
			if (d.equals(item.getDescriptor())) {
				return true;
			}
		}
		return false;
	}
	/**
	 * 
	 * @return the size of itemList
	 */
	public int getSize() {
		return size;
	}
	/**
	 * 
	 * @param index of item
	 * @return item at the ndx
	 */
	public ClothingItem getItemAt(int ndx) {
		return itemList.get(ndx);
	}

	/**
	 * Sorts the item by name
	 */
	public void sortByName() {
		itemList.sort(Comparator.comparing(ClothingItem::getDescriptor));
	}

	/**
	 * Sorts the items by condition
	 */
	public void sortByCondition() {
		itemList.sort(Comparator.comparing(ClothingItem::getCondition));
	}

	/**
	 * 
	 * @param selectedValue - descriptor
	 * @return returns the item information with the given descriptor
	 */
	public String toString(String selectedValue) {
		for (ClothingItem item : itemList) {
			if (selectedValue.equals(item.getDescriptor())) {
				return item.toString();
			}
		}
		return new String("");
	}

	/**
	 * Removes an item from the list and calls to remove from the database
	 * @param selectedValue
	 */
	public void deleteItem(String selectedValue) {
		for (int i = 0; i < size; i++) {
			if (selectedValue.equals(itemList.get(i).getDescriptor())) {
				itemList.remove(i);
				DBConnector db = new DBFactory().getDB("CLOSET");
				db.delete(selectedValue);
				size--;
			}
		}
	}
}
