package my.closet.database;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import my.closet.Closet;
import my.closet.ClothingItem;
import my.closet.HistoryList;
import my.closet.ItemHistory;
import my.closet.Outfit;
import my.closet.OutfitList;
import my.closet.logger.AbstractLogger;
import my.closet.logger.LoggerChain;

public class DBConnector {
	private static final String DB_DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
	private static final String DB_CONNECTION = "jdbc:derby:ClosetDB;create=true";
	private static final String DB_USER = "";
	private static final String DB_PASSWORD = "";


	static AbstractLogger logger = LoggerChain.getLoggers();
	
	/**
	 * returns a connection to the database
	 * @return connection
	 */
	public static Connection getDBConnection() {
		Connection dbConnection = null;
		try {
			Class.forName(DB_DRIVER);
		} catch (ClassNotFoundException e) {
			logger.log(AbstractLogger.FATAL, "Class not foumd");
		}
		try {
			dbConnection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
			return dbConnection;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return dbConnection;
	}

	public int init(HistoryList historyList) {
		return 0;
	}

	public int init(OutfitList outfitList) {
		return 0;
	}
	public int init(Closet closet) {
			return 0;
		}
	public void addOutfit(Outfit newO) {}

	public void addItem(ClothingItem i) {}

	public void delete(String selectedValue) {}

	public void update(String name, String comment, Date date) {}

	public void insert(ItemHistory newI) {}
}
