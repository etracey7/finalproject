package my.closet.database;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import my.closet.HistoryList;
import my.closet.ItemHistory;
import my.closet.logger.AbstractLogger;

public class DBHistory extends DBConnector {
	/**
	 * creates a history table
	 * @throws SQLException
	 */
	private static void createItemTable() {
		Statement statement = null;
		Connection dbConnection = null;
		String createTableSQL = "CREATE TABLE History(" + "Name VARCHAR(30), "
				+ "Comment VARCHAR(200), "
				+ "LastWorn DATE, "
				+ "PRIMARY KEY (Name) " + ")";
		try {
			dbConnection = getDBConnection();
			if (dbConnection != null) {
				statement = dbConnection.createStatement();
				statement.execute(createTableSQL);
				dbConnection.close();
				statement.close();
			}
		} catch (SQLException e) {
			logger.log(AbstractLogger.ERROR, "Table can bot ne created");
		}
	}

	/**
	 * loads the history table into a historylist
	 * @param HistoryList
	 * @return size of list
	 */
	public int init(HistoryList historyList) {
		Connection dbConnection = null;
		Statement statement = null;
		String queryTableSQL = "SELECT * FROM History";
		ItemHistory h;
		int size = 0;
		
		try {
			dbConnection = getDBConnection();
			if (dbConnection != null) {
				statement = dbConnection.createStatement();
				ResultSet rs = statement.executeQuery(queryTableSQL);
				while (rs.next()) {
					h = new ItemHistory();
					h.setLastWorn(rs.getDate("LastWorn"));
					h.setHistory(rs.getString("Comment"));
					h.setName(rs.getString("Name"));
					historyList.addItem(h);
					size++;
				}

				dbConnection.close();
			}
			if (statement != null) {
				statement.close();
			}
		} catch (SQLException e) {
			logger.log(AbstractLogger.ERROR, "Table can bot ne created");
		} 
		return size;
	}

	/**
	 * updates the database with new history
	 * @param name of the item
	 * @param comment about last worn
	 * @param date when last worn
	 */
	public void update(String name, String comment, Date lastWorn) {
		Connection dbConnection = null;
		String updateTableSQL = "UPDATE History SET Comment = ? , LastWorn = ?" 
				+ "WHERE Name = ?";
		try {
			dbConnection = getDBConnection();
			if (dbConnection != null) {
				PreparedStatement ps = dbConnection.prepareStatement(updateTableSQL);
				ps.setString( 1, comment);
				ps.setDate(2, lastWorn);
				ps.setString(3, name);
				ps.executeUpdate();
				dbConnection.close();
			}
		} catch (SQLException e) {
			logger.log(AbstractLogger.ERROR, "init failed");
		} 
	}

	/**
	 * inserts a history
	 */
	public void insert(ItemHistory newI) {
		Connection dbConnection = null;
		String insertTableSQL = "INSERT INTO History (name, Comment, LastWorn)"
				+ "VALUES (?, ?, ?)";
		try {
			dbConnection = getDBConnection();
			if (dbConnection != null) {
				PreparedStatement ps = dbConnection.prepareStatement(insertTableSQL);
				ps.setString( 1, newI.getName());
				ps.setString(2, newI.getComment());
				ps.setDate(3, new Date(newI.getLastWorn().getTime()));
				ps.executeUpdate();
				dbConnection.close();
			}
		} catch (SQLException e) {
			logger.log(AbstractLogger.ERROR, "Insert Failed");
		} 
	}

	/**
	 * delete a history by it's name
	 */
	public void delete(String name) {
		Connection dbConnection = null;
		Statement statement = null;
		String deleteTableSQL = "DELETE FROM History WHERE Name = '" + name + "'";
		try {
			dbConnection = getDBConnection();
			if (dbConnection != null) {
				statement = dbConnection.createStatement();
				if (statement != null) {
					statement.executeUpdate(deleteTableSQL);
					statement.close();
				}
				dbConnection.close();
			}
		} catch (SQLException e) {
			logger.log(AbstractLogger.ERROR, "Delete failed");
		} finally {
			
		}
	}

}
