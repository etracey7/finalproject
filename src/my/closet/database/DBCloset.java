package my.closet.database;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import my.closet.Closet;
import my.closet.ClothingItem;
import my.closet.logger.AbstractLogger;

/**
 * This class is responsible for manipulating the database with
 * 	clothing items
 * 
 * @author Emily
 *
 */
public class DBCloset extends DBConnector {
	
	/**
	 * creates an item table
	 */
	private static void createItemTable() {
		Connection dbConnection = null;
		Statement statement = null;
		String createTableSQL = "CREATE TABLE Item(" + "Name VARCHAR(30), "
				+ "Type VARCHAR(20) NOT NULL, "
				+ "Color VARCHAR(20) NOT NULL, " 
				+ "Brand VARCHAR(20) NOT NULL, "
				+ "Condition VARCHAR(20) NOT NULL, "
				+ "PRIMARY KEY (Name) " + ")";
		try {
			dbConnection = getDBConnection();
			if (dbConnection != null) {
				statement = dbConnection.createStatement();
				if (statement != null) {
					statement.execute(createTableSQL);
					logger.log(AbstractLogger.DEBUG, "Table confirmed");
					statement.close();
				}
				dbConnection.close();
			} else {
				logger.log(AbstractLogger.FATAL, "Could not connect to database");
			}
		} catch (SQLException e) {
			logger.log(AbstractLogger.ERROR, "item table could not be created or already exists");
		} 
	}
	/**
	 * loads the database items into the closet
	 * @param closet
	 */
	public int init(Closet c) {
		Connection dbConnection = null;
		Statement statement = null;
		String queryTableSQL = "SELECT * FROM Item";
		ClothingItem newItem = new ClothingItem();
		int size = 0;
		
		try {
			dbConnection = getDBConnection();
			if (dbConnection != null) {
				statement = dbConnection.createStatement();
				if (statement != null) {
					createItemTable();
					ResultSet rs = statement.executeQuery(queryTableSQL);
					while (rs.next()) {
						newItem = new ClothingItem();
						newItem.setDescriptor(rs.getString("Name"));
						newItem.setBrand(rs.getString("Brand"));
						newItem.setColor(rs.getString("Color"));
						newItem.setType(rs.getString("Type"));
						newItem.setCondition(rs.getString("Condition"));
						c.addListItem(newItem);
						logger.log(AbstractLogger.DEBUG, "item loaded: " + newItem.getDescriptor());
						size++;
					}
					statement.close();
				}
				dbConnection.close();
			}
		} catch (SQLException e) {
			logger.log(AbstractLogger.ERROR, "Query could not be completed");
		}
		return size;
	}
	/**
	 * inserts an item into the database
	 * @param ClothingItem to be inserted
	 */
	public void addItem(ClothingItem i) {
		Connection dbConnection = null;
		String insertTableSQL = "INSERT INTO Item" 
				+ " (Name, Brand, Color, Condition, Type) " 
				+ "VALUES" + "(?,?,?,?,?)";
		try {
			dbConnection = getDBConnection();
			if (dbConnection != null) {
				PreparedStatement ps = dbConnection.prepareStatement(insertTableSQL);
				ps.setString( 1, i.getDescriptor());
				ps.setString(2, i.getBrand());
				ps.setString(3, i.getColor());
				ps.setString(4, i.getCondition());
				ps.setString(5, i.getType());
				ps.executeUpdate();
				dbConnection.close();
				ps.close();
				logger.log(AbstractLogger.DEBUG, "item inserted: "+ i.getDescriptor());
			}
		} catch (SQLException e) {
			logger.log(AbstractLogger.ERROR, "Insert into item failed");
		}
	}
	/**
	 * this functions search for an item
	 * @param ClothingItem being searched for
	 * @return true if item is in database
	 */
	public boolean search(ClothingItem i) {
		Connection dbConnection = null;
		Statement statement = null;
		boolean found = false;
		String queryTableSQL = "SELECT Name From Item " 
				+ "WHERE Name = ?";
		try {
			dbConnection = getDBConnection();
			PreparedStatement ps = dbConnection.prepareStatement(queryTableSQL);
			ps.setString( 1, i.getDescriptor());
			ResultSet rs = ps.executeQuery();
			while (rs.next()){
				found = true;
			}
			dbConnection.close();
			ps.close();
		} catch (SQLException e) {
			System.out.print(e.getMessage());
		}
		return found;
	}

	/**
	 * this function removes am item from the database
	 * @param name of item to be deleted
	 */
	public void delete(String selectedValue) {
		Connection dbConnection = null;
		Statement statement = null;
		boolean ifInsert = true;
		String deleteTableSQL = "DELETE FROM Item WHERE Name = '" + selectedValue + "'"; 
		try {
			dbConnection = getDBConnection();
			if (dbConnection != null) {
			statement = dbConnection.createStatement();
				if (statement != null) {
					statement.executeUpdate(deleteTableSQL);
					statement.close();
				}
			}
			if (dbConnection != null) {
				dbConnection.close();
			}
		} catch (SQLException e) {
			logger.log(AbstractLogger.ERROR, "Delete from item failed");
		}
	}
}
