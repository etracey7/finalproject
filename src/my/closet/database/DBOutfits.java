package my.closet.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import my.closet.Outfit;
import my.closet.OutfitList;
import my.closet.logger.AbstractLogger;

/**
 * This class handles database manipulation for outfits
 * @author Emily
 *
 */
public class DBOutfits extends DBConnector {
	/**
	 * creates an item table
	 * @throws SQLException
	 */
	private static void createOutfitTable() throws SQLException {
		Connection dbConnection = null;
		Statement statement = null;
		String createTableSQL = "CREATE TABLE Outfit(" + "Name VARCHAR(30), "
				+ "ItemName VARCHAR(30) NOT NULL) ";
		
		try {
			dbConnection = getDBConnection();
			statement = dbConnection.createStatement();
			statement.execute(createTableSQL);
		} catch (SQLException e) {
			logger.log(AbstractLogger.ERROR, "Table can not ne created");
		} finally {
			if (statement != null) {
				statement.close();
			}
			if (dbConnection != null) {
				dbConnection.close();
			}
		}
	}
	/**
	 * initializes an outfitList
	 */
	public int init(OutfitList ol) {
		Connection dbConnection = null;
		Statement statement = null;
		String queryTableSQL = "SELECT * FROM Outfit";
		int count = 0;
		
		try {
			dbConnection = getDBConnection();
			if (dbConnection != null) {
				statement = dbConnection.createStatement();
				if (statement != null) {
					//create the tale if needed
					createOutfitTable();
					//get all outfits from the table
					ResultSet rs = statement.executeQuery(queryTableSQL);
					while (rs.next()) {
						String outfitName = rs.getString("Name");
						String outfitItem = rs.getString("ItemName");
						
						if (ol.find(outfitName)) {
							ol.setWithName(outfitName, outfitItem);
						} else {
							ol.setNew(outfitName,outfitItem);
							count++;
						}
					}
					statement.close();
				}
				dbConnection.close();
			}
		} catch (SQLException e) {
			logger.log(AbstractLogger.ERROR, "init failed");
		} 
		return count;
	}
	
	public void addOutfit(Outfit o) {
		Connection dbConnection = null;
	
		try {
			dbConnection = getDBConnection();
			if (dbConnection != null) {
				for (int ndx = 0; ndx < o.getItemList().size(); ndx++) {
					String insertTableSQL = "INSERT INTO Outfit" 
							+ "(Name, ItemName) " 
							+ "VALUES" + "( ?, '"
							+ o.getItemList().get(ndx) + "' )";
					PreparedStatement ps = dbConnection.prepareStatement(insertTableSQL);
					ps.setString( 1, o.getTitle());
					ps.execute();
					ps.close();
				}
				dbConnection.close();
			}
		} catch (SQLException e) {
			System.out.print(e.getMessage());
		} finally {
		
		}
	}
	public void deleteOutfit(String name) {
		Connection dbConnection = null;
		Statement statement = null;
	
		try {
			dbConnection = getDBConnection();
			if (dbConnection != null) {
				statement = dbConnection.createStatement();
				if (statement != null) {
					String deleteSql = "DELETE FROM Outfit WHERE Name = '" +
							name + "'";
					statement.execute(deleteSql);
					statement.close();
				}
				dbConnection.close();
			}
		} catch (SQLException e) {
			
		} finally {
		
		}
	}
}
