package my.closet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import my.closet.database.DBConnector;
import my.closet.database.DBOutfits;
import my.closet.factory.DBFactory;

/**
 * represents all of the outfits in a closet
 * @author Emily
 *
 */
public class OutfitList {
	List<Outfit> outfits;
	int size;
	
	
	/**
	 * loads all outfits from the database
	 */
	public void load() {
		outfits = new ArrayList<Outfit>();
		DBConnector db = new DBFactory().getDB("OUTFITS");
		size = db.init(this);	
	}

	/**
	 * adds an item name to the outfit given
	 * @param outfitName
	 * @param outfitItem
	 */
	public void setWithName(String outfitName, String outfitItem) {
		for (Outfit o: outfits) {
			if (o.getTitle().equals(outfitName)) {
				o.addItem(outfitItem);
			}
		}
	}

	/**
	 * searches for the occurrence of an outfit name in the list
	 * @param outfitName
	 * @return true if name is in list
	 */
	public boolean find(String outfitName) {
		for (Outfit o: outfits) {
			if (o.getTitle().equals(outfitName)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * adds a new outfit to the list, called by the database class
	 * @param outfitName
	 * @param outfitItem
	 */
	public void setNew(String outfitName, String outfitItem) {
		Outfit o = new Outfit();
		o.setTitle(outfitName);
		o.addItem(outfitItem);
		outfits.add(o);
	}
	
	/**
	 * returns the values for each outfit in the list
	 */
	public String toString() {
		String result = "";
		for (Outfit o: outfits) {
			result += o.toString();
		}
		return result;
	}

	/**
	 * adds a new outfit to the database and the list
	 * @param title of the outfit
	 * @param items
	 */
	public void addNew(String title, String[] items) {
		Outfit newO = new Outfit();
		newO.setItemList(Arrays.asList(items));
		newO.setTitle(title);
		DBConnector db = new DBFactory().getDB("OUTFITS");
		db.addOutfit(newO);
		outfits.add(newO);
	}
}
