package my.closet;

import java.util.Date;

/**
 * represents a history for a clothing item
 * @author Emily
 *
 */
public class ItemHistory {
	private String name;
	private String comment;
	private Date lastWorn;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getComment() {
		return comment;
	}
	public void setHistory(String comment) {
		this.comment = comment;
	}
	public Date getLastWorn() {
		return lastWorn;
	}
	public void setLastWorn(Date lastWorn) {
		this.lastWorn = lastWorn;
	}
	
	
}
