package my.closet.logger;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Timestamp;

/**
 * logs all errors
 * @author Emily
 *
 */
public class ErrorLogger extends AbstractLogger {
	String filename = "ErrorLog.txt";
	
	/**
	 * assigns level
	 * @param level
	 */
	public ErrorLogger(int level) {
		this.level = level;
	}
	
	@Override
	protected void write(String m) {
		try {
			FileWriter writer = new FileWriter(filename,true);
			BufferedWriter out = new BufferedWriter(writer);
			out.write("New log:");
			out.write(m+" - " + new Timestamp(System.currentTimeMillis()));
			out.newLine();
			if (out != null ) {
				out.close();
			}
		} catch (IOException e) {
			
		}
	}
}
