package my.closet.logger;

/**
 * this class abstracts a logger
 * @author Emily
 *
 */
public abstract class AbstractLogger {
	public static int INFO = 1;
	public static int DEBUG = 2;
	public static int ERROR = 3;
	public static int FATAL = 4;
	
	protected int level;
	
	protected AbstractLogger next;
	
	/**
	 * assigns next logger
	 * @param next
	 */
	public void setNext(AbstractLogger next) {
		this.next = next;
	}
	/**
	 * logs a message by writing or delegating to next logger
	 * @param level
	 * @param m
	 */
	public void log(int level, String m) {
		if (this.level <= level) {
			write(m);
		}
		if (next != null) {
			next.log(level, m);
		}
	}
	/**
	 * writes the message to a file
	 * @param message to be logged
	 */
	abstract protected void write(String m);
}
