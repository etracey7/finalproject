package my.closet.logger;

/**
 * implements the chain of responsabilities of the loggers
 * @author Emily
 *
 */
public class LoggerChain {
	
	/**
	 * return chain of loggers
	 * @return loggers connected to each other
	 */
	public static AbstractLogger getLoggers(){

	      AbstractLogger errorLogger = new ErrorLogger(AbstractLogger.ERROR);
	      AbstractLogger changeLogger = new ChangeLogger(AbstractLogger.DEBUG);
	      AbstractLogger infoLogger = new InfoLogger(AbstractLogger.INFO);

	      errorLogger.setNext(changeLogger);
	      changeLogger.setNext(infoLogger);

	      return errorLogger;	
	   }
}
