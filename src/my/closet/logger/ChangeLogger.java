package my.closet.logger;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Timestamp;
/**
 * logs debug changes
 * @author Emily
 *
 */
public class ChangeLogger extends AbstractLogger {
	String filename = "DataChangesLog.txt";
	
	/**
	 * changes the logger
	 * @param level
	 */
	public ChangeLogger(int level) {
		this.level = level;
	}
	
	@Override
	protected void write(String m) {
		try {
			FileWriter writer = new FileWriter(filename,true);
			BufferedWriter out = new BufferedWriter(writer);
			out.write("New log:");
			out.write(m+" - " + new Timestamp(System.currentTimeMillis()));
			out.newLine();
			if (out != null ) {
				out.close();
			}
		} catch (IOException e) {
			
		}
	}
}
