package my.closet.logger;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Timestamp;

public class InfoLogger extends AbstractLogger {

	String filename = "InformationLog.txt";
	
	/**
	 * assigns level
	 * @param level
	 */
	public InfoLogger(int level) {
		this.level = level;
	}
	
	@Override
	protected void write(String m) {
		try {
			FileWriter writer = new FileWriter(filename,true);
			BufferedWriter out = new BufferedWriter(writer);
			out.write("New log:");
			out.write(m+" - " + new Timestamp(System.currentTimeMillis()));
			out.newLine();
			if (out != null ) {
				out.close();
			}
		} catch (IOException e) {
			System.out.print("error");
		}
	}
	
}
