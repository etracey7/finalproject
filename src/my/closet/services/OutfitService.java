package my.closet.services;

import my.closet.OutfitList;
import my.closet.logger.AbstractLogger;
import my.closet.logger.LoggerChain;

/**
 * This class services an outfit list
 * @author Emily
 *
 */
public class OutfitService {
	private static OutfitList outfits = null;
	private static OutfitService service = null;
	
	static AbstractLogger logger = LoggerChain.getLoggers();
	
	/**
	 * implementation of a singleton
	 * @return
	 */
	public static OutfitService getInstance() {
		if (service == null) {
			service = new OutfitService();
			outfits = new OutfitList();
			outfits.load();
			logger.log(AbstractLogger.INFO,"Outfits Loaded");

		} 
		return service;
	}
	/**
	 * returns all information on the outfits
	 * @return string on all information
	 */
	public String getListNames() {
		return outfits.toString();
	}
	/**
	 * inserts a new outfit with given fields
	 * @param title
	 * @param items - list of item names
	 */
	public void insertNewOutfit(String title, String[] items) {
		outfits.addNew(title, items);
		logger.log(AbstractLogger.INFO,"Outfit added "+title);
	}
}
