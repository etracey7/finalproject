package my.closet.services;

import java.util.Date;

import my.closet.HistoryList;
import my.closet.logger.AbstractLogger;
import my.closet.logger.LoggerChain;

/**
 * this class services a history list
 * @author Emily
 *
 */
public class HistoryService {
	
	private static HistoryList histories;
	private static HistoryService service = null;
	
	static AbstractLogger logger = LoggerChain.getLoggers();

	/**
	 * implementation of a singleton
	 * @return
	 */
	public static HistoryService getInstance() {
		if (service == null) {
			service = new HistoryService();
			histories = new HistoryList();
			histories.load();
			logger.log(AbstractLogger.INFO,"Histories Loaded");
		} 
		return service;
	}

	/**
	 * returns information of all histories
	 * @return
	 */
	public String getListNames() {
		return histories.toString();
	}
	/**
	 * either inserts a new item in the histories or updates an existing one
	 * @param name of item
	 * @param comment
	 * @param lastWorn
	 */
	public void insertOrUpdate(String name, String comment, Date lastWorn) {
		if (!histories.find(name)) {
			histories.addItem(name, comment, lastWorn);
			logger.log(AbstractLogger.DEBUG,name+" added new history");
		} else {
			histories.update(name, comment, lastWorn);
			logger.log(AbstractLogger.DEBUG,name+" updated history");

		}
	}

	/**
	 * deletes an item with the given name
	 * @param name
	 */
	public void deleteItemHisory(String name) {
		histories.deleteHistory(name);
		logger.log(AbstractLogger.DEBUG, "deleted history for "+name);
	}
}
