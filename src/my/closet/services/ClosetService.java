package my.closet.services;

import my.closet.Closet;
import my.closet.ClothingItem;
import my.closet.logger.AbstractLogger;
import my.closet.logger.LoggerChain;

/**
 * This class services the closet class and connects to the graphics
 * @author Emily
 *
 */
public class ClosetService {
	
	private static Closet closet = new Closet();
	private static ClosetService service = null;
	
	static AbstractLogger logger = LoggerChain.getLoggers();
	
	/**
	 * Singleton implementation
	 * @return
	 */
	public static ClosetService getInstance() {
		if (service == null) {
			service = new ClosetService();
			closet.load();
			logger.log(AbstractLogger.INFO,"Closet Loaded");
		} 
		return service;
	}
	/**
	 * Insert an item with the given strings coresponding to fields in an item
	 * @param name
	 * @param brand
	 * @param color
	 * @param type
	 * @param condition
	 */
	public void insertItem(String name, String brand, String color, String type, String condition) {
		ClothingItem i = new ClothingItem(type, name, color, brand, condition);
		closet.addItem(i);
		logger.log(AbstractLogger.DEBUG, "Insert Item " + name);
	}
	/**
	 * returns the title of an item at the given 
	 * @param index
	 * @return item descriptor
	 */
	public String getItemTitle(int ndx) {
		String title = closet.getItemAt(ndx).getDescriptor();
		return title;
	}
	/**
	 * returns the number of items in the closet
	 * @return int - based on size
	 */
	public int getNumber() {
		return closet.getSize();
	}

	/**
	 * calls the closet to sort its items
	 */
	public void sortByName() {
		closet.sortByName();
		logger.log(AbstractLogger.INFO,"Closet Sorted by name");
	}

	/**
	 * calls the closet to group by condition
	 */
	public void sortByCondition() {
		closet.sortByCondition();
		logger.log(AbstractLogger.INFO,"Closet sorted by condition");
	}

	/**
	 * gives the information corresponding to the item descriptor
	 * @param selectedValue - descriptor
	 * @return all item information
	 */
	public String toString(String selectedValue) {
		return closet.toString(selectedValue);
	}

	/**
	 * deletes an item in the closet
	 * @param selectedValue
	 */
	public void deleteItem(String selectedValue) {
		closet.deleteItem(selectedValue);
		logger.log(AbstractLogger.DEBUG,"deleted"+selectedValue);
	}

	/**
	 * checks to see if an inserted descriptor is valid
	 * @param d
	 * @return true if descriptor is valid
	 */
	public boolean validCheckDescriptor(String d) {
		if (d == null) {
			return false;
		}
		return !closet.find(d);
	}
}

