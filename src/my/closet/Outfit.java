
package my.closet;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * represents an outfit with a name and list of clothing item names
 * @author Emily
 *
 */
public class Outfit {
	private List<String> itemList = new ArrayList<String>();
	private String title;
	
	public void addToList(String itemName) {
		itemList.add(itemName);
	}
	public List<String> getItemList() {
		return itemList;
	}
	public void setItemList(List<String> itemList) {
		this.itemList = itemList;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public void addItem(String outfitItem) {
		itemList.add(outfitItem);
	}
	public String toString() {
		String result = "";
		result += "Title: " + title + "\nItems:";
		for (String str: itemList) {
			result += str + ", ";
		}
		return result+"\n\n";
	}
}
