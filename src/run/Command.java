package run;

import java.awt.EventQueue;

import my.closet.graphics.ClosetFrame;
import my.closet.graphics.OpeningFrame;

/**
 * starts the first frame
 * @author Emily
 *
 */
public class Command {
	/**
	 * starts the frames
	 */
	public static void execute() {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				ClosetFrame frame = new ClosetFrame();
				frame.setVisible(true);
				OpeningFrame open = new OpeningFrame();
				open.setVisible(true);
			}
			
		});
		
	}
}
