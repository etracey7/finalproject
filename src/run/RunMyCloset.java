package run;

/**
 * run this class to run project
 * @author Emily
 *
 */
public class RunMyCloset {
	public static void main(String[] args) {
		Command.execute();
	}
}
